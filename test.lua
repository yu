setmetatable(_G,{__index=function(t,k) error("undefined symbol:"..k,2) end})
require "yu.yu"

local t0=os.clock()

local builder1=yu.newBuilder()
local m=builder1:build('test/test4.lx')
-- local codegen=yu.newCodeGenerator()
-- codegen:visitNode(m)
local code=yu.codegen(m)
local t1=os.clock()

print "--------------------------"
print("time elapsed:",((t1-t0)*1000)..'ms')
print "---------------generated code-------------"
print(code)
print "---------------execution-------------"
local f=loadstring(code)
local a,b=pcall(f)
print "--------------------------"
print(a,b)




-- local m=yu.parseFile('test/test3.lx')
-- local c=yu.newDeclCollector()
-- c:visitNode(m)

-- local r=yu.newResolver()
-- r:visitNode(m)


