
require "yu.utils"

module("yu",package.seeall)

local visitor={}
local paths

local is,isConst=yu.is,yu.isConst
local ipairs=ipairs
local _doVisitCallback

local function visitNode(vi,n,list)
	local vit=getmetatable(vi)
	if not vit or  vit.__index~=visitor then
		print(vi,vit)
		error("Internal error!")
	end
	
	vi.depth=vi.depth+1
	local parentNode=vi.currentNode
	vi.currentNode=vi.nodeStack:push(n)
	
	local tag=n.tag
	
	local pre=vi.pre
	if pre then
		local c=pre['any']
		if c then 
			if _doVisitCallback(vi,n,list,parentNode,c) then return true end	
		end
		
		local c=pre[tag]
		if c then 
			if _doVisitCallback(vi,n,list,parentNode,c) then return true end	
		end
	end
	
	if not isConst(n) then
		local p=paths[tag]
		if p==nil then error("no visit path for "..tag) end
		if p then p(vi,n) end 
	end
	
	local post=vi.post
	if post then
		local c=post[tag]
		if c then 
			if _doVisitCallback(vi,n,list,parentNode,c) then return true end	
		end
		
		local c=post['any']
		if c then 
			if _doVisitCallback(vi,n,list,parentNode,c) then return true end	
		end
	end
	vi.depth=vi.depth-1
	vi.currentNode=vi.nodeStack:pop()
	
	return true
end

local function visitEachNode(vi,l)
	if not l then return end
	for i,n in ipairs(l) do
		visitNode(vi,n,l)
	end
end

local function overwriteTable(t1,t2) --t2 -> t1
	for k in pairs(t1) do
		t1[k]=nil
	end
	
	for k,v in pairs(t2) do
		t1[k]=v
	end
end

_doVisitCallback= function (vi,n,list,parentNode,callback)
	local msg,data=callback(vi,n,parentNode)
	if msg=='replace' then
		-- local p=list or parentNode
		-- for k,v in pairs(p) do
			-- if v==n then
				-- p[k]=data
			-- end
		-- end
		
		
		data.p0=n.p0
		data.p1=n.p1
		data.module=n.module
		
		
		-- return visitNode(vi,data,list)
		
		overwriteTable(n,data)
		vi.currentNode=vi.nodeStack:pop()
		vi.depth=vi.depth-1
		return visitNode(vi,n,list)
		
	elseif msg=='skip' then
		vi.currentNode=vi.nodeStack:pop()
		vi.depth=vi.depth-1
		return true
	end
	
	return false
end


function newVisitor(data)
	data=data or {}
	data.depth=0
	data.nodeStack=yu.newStack()
	data.currentNode=false
	data.currentBlock=false
	data.currentFunc=false
	
	return setmetatable(data,{__index=visitor})
end

visitor.visitNode=visitNode
visitor.visitEachNode=visitEachNode


function visitor:err(msg,token,module)
	return yu.compileErr(msg,token,module)
end

function visitor:findParentLoop()
	local s=self.nodeStack.stack
	for i=#s,1,-1 do
		local n=s[i]
		local t=n.tag
		if is(t,'whilestmt','forstmt','foreachstmt') then return n end
		if is(t,'funcdecl','methoddecl') then break end
	end
	return nil
end

function visitor:findParentClass()
	local s=self.nodeStack.stack
	for i=#s,1,-1 do
		local n=s[i]
		local t=n.tag
		if is(t,'classdecl','externclass') then return n end
		if is(t,'funcdecl') then break end
	end
	return nil
end


function visitor:findParentFunc()
	local s=self.nodeStack.stack
	for i=#s,1,-1 do
		local n=s[i]
		local t=n.tag
		if is(t,'funcdecl','methoddecl','closure') then return n end
	end
	return nil
end


paths={
	module=function(vi,m)
		local lastModule=vi.currentModule
		vi.currentModule=m
		visitNode(vi,m.block)
		vi.currentModule=lastModule
	end,
	
	block=function(vi,b)
		local pb=vi.currentBlock
		vi.currentBlock=b
		visitEachNode(vi,b)
		vi.currentBlock=pb
	end,
	
	import=function(vi,im)
		--TODO:...
	end,
	
	private=false,
	public=false,
	rawlua=false,
	
	typeref=false,
	
	ttype=function(vi,tt)
		visitEachNode(vi,tt.args)
	end,
	
	type=function(vi,t)
		--TODO:...
	end,
	
	tabletype=function(vi,t)
		if t.ktype then visitNode(vi,t.ktype) end
		visitNode(vi,t.etype)
	end,
	
	functype=function(vi,ft)
		if ft.args then visitEachNode(vi,ft.args) end
		if ft.rettype then 
			visitNode(vi,ft.rettype)
		end
	end,
	
	mulrettype=function(vi,mr)
		visitEachNode(vi,mr.types)
	end,
	
	dostmt=function(vi,d)
		visitNode(vi,d.block)
	end,
	
	ifstmt=function(vi,i)
		visitNode(vi,i.cond)
		visitNode(vi,i.body.thenbody)
		if i.body.elsebody then visitNode(vi,i.body.elsebody) end
	end,
	
	switchstmt=function(vi,s)
		visitNode(vi,s.cond)
		if s.default then visitNode(vi,s.default) end
		visitEachNode(vi,s.cases)
	end,
	
	case=function(vi,c)
		visitEachNode(vi,c.conds)
		visitNode(vi,c.block)
	end,
	
	returnstmt=function(vi,r)
		if r.values then visitEachNode(vi,r.values) end
	end,
	
	yieldstmt=function(vi,y)
		if y.values then visitEachNode(vi,y.values) end
	end,
	
	forstmt=function(vi,f)
		visitNode(vi,f.var)
		visitEachNode(vi,f.range)
		visitNode(vi,f.block)
	end,
	
	foreachstmt=function(vi,f)
		visitEachNode(vi,f.vars)
		visitNode(vi,f.block)
	end,
	
	whilestmt=function(vi,w)
		visitNode(vi,w.cond)
		visitNode(vi,w.block)
	end,
	
	continuestmt=false,
	breakstmt=false,
	
	trystmt=function(vi,t)
		visitNode(vi,t.block)
		visitEachNode(vi,t.catches)
	end,
	
	throwstmt=function(vi,t)
		visitEachNode(vi,t.values)
	end,
	
	catch=function(vi,c)
		visitEachNode(vi,c.vars)
		visitNode(vi,c.block)
	end,
	
	assignstmt=function(vi,a)
		visitEachNode(vi,a.vars)
		visitEachNode(vi,a.values)
	end,
	
	assopstmt=function(vi,a)
		if a.vars then visitEachNode(vi,a.vars) end
		visitEachNode(vi,a.values)
	end,
	
	batchassign=function(vi,a)
		visitNode(vi,a.var)
		--TODO!!!
		visitEachNode(vi,a.values)
	end,
	
	exprstmt=function(vi,e)
		visitNode(vi,e.expr)
	end,
	
	vardecl=function(vi,vd)
		visitEachNode(vi,vd.vars)
		if vd.values then visitEachNode(vi,vd.values) end
	end,
	
	tvar=function(vi,v)
		---TODO
	end,
	
	var=function(vi,v)
		if v.type then visitNode(vi,v.type) end
		if v.value then visitNode(vi,v.value) end
	end,
	
	arg=function(vi,v)
		if v.type then visitNode(vi,v.type) end
		if v.value then visitNode(vi,v.value) end
	end,
	
	enumdecl=function(vi,e)
		visitEachNode(vi,e.items)
	end,
	
	enumitem=function(vi,v)
		if v.value then visitNode(vi,v.value) end
	end,
	
	classdecl=function(vi,c)
		if c.tvars then
			visitEachNode(vi,c.tvars)
		end
		if c.decls then
			visitEachNode(vi,c.decls)
		end
	end,
	
	methoddecl=function(vi,m)
		local pf=vi.currentFunc
		vi.currentFunc=m
		
		visitNode(vi,m.type)
		if m.block then visitNode(vi,m.block) end
		
		vi.currentFunc=pf
	end,
	
	funcdecl =function(vi,f)
		local pf=vi.currentFunc
		vi.currentFunc=f
		
		visitNode(vi,f.type)
		if not f.extern then visitNode(vi,f.block) end
		
		vi.currentFunc=pf
	end,
	
	exprbody=function(vi,e)
		visitNode(vi,e.expr)
	end,
	
	varacc=false,
	
	call=function(vi,c)
		visitNode(vi,c.l)
		if c.args then visitEachNode(vi,c.args) end
	end,
	
	spawn=function(vi,s)
		visitNode(vi,s.proto)
	end,
	
	new=function(vi,n)
		visitNode(vi,n.class)
		if n.args then visitEachNode(vi,n.args) end
	end,
	
	binop=function(vi,b)
		visitNode(vi,b.l)
		visitNode(vi,b.r)
	end,
	
	unop=function(vi,u)
		visitNode(vi,u.l)
	end,
	
	ternary=function(vi,t)
		visitNode(vi,t.l)
		visitNode(vi,t.vtrue)
		visitNode(vi,t.vfalse)
	end,
	
	member=function(vi,m)
		visitNode(vi,m.l)
	end,
	
	index=function(vi,i)
		visitNode(vi,i.l)
		if i.key then visitNode(vi,i.key) end
		--todo:key?
	end,
	
	table=function(vi,t)
		if t.items then
			visitEachNode(vi,t.items)
		end
	end,
	
	item=function(vi,i)
		visitNode(vi,i.key)
		visitNode(vi,i.value)
	end,
	
	seq=function(vi,t)
		if t.items then
			visitEachNode(vi,t.items)
		end
	end,
	
	cast=function(vi,a)
		visitNode(vi,a.l)
		visitNode(vi,a.dst)
	end,
	
	is=function(vi,a)
		visitNode(vi,a.l)
		visitNode(vi,a.dst)
	end,
	
	closure=function(vi,c)
		visitNode(vi,c.type)
		visitNode(vi,c.block)
	end,
	
	self=false,
	super=false,
	
	extern=function(vi,ex)
		visitEachNode(vi,ex.decls)
	end,
	
	externfunc=function(vi,f)
		visitNode(vi,f.type)
	end,
	
	externclass=function(vi,c)
		if c.decls then
			visitEachNode(vi,c.decls)
		end
	end,
	
	-----------TYPES
	voidtype=false,
	numbertype=false,
	stringtype=false,
	booleantype=false
}

