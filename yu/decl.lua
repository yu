require "yu.visitor"
require "yu.type"

local is=yu.is
local getTypeDecl,newTypeRef=yu.getTypeDecl,yu.newTypeRef
local ipairs,print=ipairs,print

module("yu",package.seeall)

local pre={}
local post={}

local isConst,isDecl,isGlobalDecl=isConst,isDecl,isGlobalDecl
local is=yu.is
local function makeNamePrefix(stack)
	local out='_yu'
	for i,n in ipairs(stack) do
		if out then
			out=out..'_'..n
		else
			out=n
		end
	end
	return out or ''
end

function newDeclCollector()
	
	return yu.newVisitor({
		pre=pre,
		post=post,
		
		nameStack=yu.newStack(),
		scopeStack=yu.newStack(),
		
		currentNamePrefix="",
		currentScope=false,
		
		pushName=function(self,name)
			self.nameStack:push(name)
			self.currentNamePrefix=makeNamePrefix(self.nameStack.stack)
		end,
		
		popName=function(self)
			self.nameStack:pop()
			self.currentNamePrefix=makeNamePrefix(self.nameStack.stack)
		end,
		
		pushScope=function(self)
			local scope={}
			self.scopeStack:push(scope)
			self.currentScope=scope
			self.currentNode.scope=scope
			return scope
		end,
		
		popScope=function(self)
			self.currentScope=self.scopeStack:pop()
			return self.currentScope
		end,
		
		addEachDecl=function(self,list)
			for i,n in ipairs(list) do
				if isDecl(n) or is(n.tag,"public","private") then 
					self:addDecl(n) 
				end
			end
		end,
		
		addDecl=function(self,decl)
			local currentScope=self.currentScope
			
			if isBuiltinType(decl.name) then
				self:err(decl.name..' is builtin type',decl,self.currentModule)
			end
			
			local tag=decl.tag
		
			if tag=="private" then
				currentScope.private=true
				return
			elseif tag=="public" then
				currentScope.private=false
				return
			end
			
			if tag=="vardecl" then
				for i,var in ipairs(decl.vars) do
					var.vtype=decl.vtype
					self:addDecl(var)
				end
				return
			end
			
			if tag=="import" then
				decl.name=decl.alias
			end
			
			local scope=currentScope
			decl.private=scope.private
			
			local name=decl.name
			local decl0=scope[name]
			
			if decl0 then  --duplicated?
				local tag0=decl0.tag
				if tag==tag0 and is(tag,"funcdecl","methoddecl") then
					--TODO:function overload
					if decl0.private ~= decl.private then
						self:err('function overload must be both private or public',decl)
					end
					
					local dd=decl0
					local n=1
					while true do
						local d1=dd.nextProto
						if not d1 then
							dd.nextProto=decl
							break
						end
						n=n+1
						dd=d1
					end
					decl.protoId=n
				else
					self:err(
						"duplicated declaration:'"..decl.name
						.."',first defined at"
						..getTokenPosString(decl0,self.currentModule)
						,decl,self.currentModule)
				end
			else
				scope[decl.name]=decl
			end
			
			if decl.alias then
				decl.fullname=decl.alias
			else
				if decl.extern then
					decl.fullname=decl.name
				else
					local currentNamePrefix=self.currentNamePrefix
					
					if currentNamePrefix~='' then
						decl.fullname=currentNamePrefix..'_'..name
					else
						decl.fullname=name
					end
					
					if decl.protoId then
						decl.fullname='_'..decl.protoId..decl.fullname
					end
					
				end
			end
			
			
			
			-- print("..added decl:",decl.tag,decl.name,decl.fullname,decl.vtype)
		end
		
		}
	)
end


-------------
function pre.any(vi,n)
	if vi.currentModule then 
		n.module=vi.currentModule 
	end
end

function pre.module(vi,m)
	vi:pushScope()
	vi:pushName(m.name)
	vi.currentModule=m
	m.module=m
end

function post.module(vi,m)
	vi:popName()
	vi:popScope()
	
end

---------------

function pre.block(vi,b,parent)
	-- if not is(parent.tag,'funcdecl','methoddecl','forstmt','catch','foreachstmt')
		-- b.scope=vi:pushScope()
	-- end
	vi:addEachDecl(b)
	
	local pn=vi.nodeStack:peek(1)
	if pn.tag=='module' then -- top block? add named imported module into scope
		for i,h in ipairs(pn.heads) do
			if h.tag=='import' and h.alias then
				h.type=moduleMetaType
				vi:addDecl(h)
			end
		end
	end
	
end

function post.block(vi,b)
	-- vi:popScope()
end

function pre.dostmt(vi,d)
	vi:pushScope()
end

function post.dostmt(vi,d)
	vi:popScope()
end

-----------

function pre.extern(vi,e)
	for i,s in ipairs(e.decls) do
		if isDecl(s) or is(s.tag,"public","private") then 
			vi:addDecl(s)
		end
	end
end

-----------


local function spreadVarTypes(vars,vardecl)
	local lasttype=nil
	for i=#vars,1,-1 do
		local var=vars[i]
		if var.type then
			lasttype=var.type
		elseif lasttype then
			var.type=lasttype
		else
			-- table.foreach(var,print)
			return yu.compileErr("type identifier expected",var,vardecl.module)
		end
	end
end


function pre:vardecl(vd)
	local vars=vd.vars
	if vd.def then --should no type id
		for i,var in ipairs(vars) do
			if var.type then return self:err("unnecessary type identifier",var.type) end
		end
		--todo:mulitple return
		for i,var in ipairs(vars) do
			local v=vd.values[i]
			var.value=v
			var.type=newTypeRef(v)
		end
		
	else
		spreadVarTypes(vars,vd)
		--todo:mulitple return
		if vd.values then
			for i,var in ipairs(vars) do
				var.value=vd.values[i]
			end
		end
		
	end
	return true
end



function pre.funcdecl(vi,f)
	vi:pushName(f.name)
	vi:pushScope()
end

function post.funcdecl(vi,f)
	vi:popScope()
	vi:popName()
end

function pre.closure(vi,f)
	f.name='C'
	vi:pushName(f.name)
	vi:pushScope()
end

function post.closure(vi,f)
	vi:popScope()
	vi:popName()
end


-----------
function pre.methoddecl(vi,f,parent)
	vi:pushName(f.name)
	vi:pushScope()
	if f.abstract then 
		parent.abstract=true
	end
end

function post.methoddecl(vi,f)
	
	vi:popScope()
	vi:popName()
end

-----------
function pre.externfunc(vi,f)
	vi:pushName(f.name)
	vi:pushScope()
end

function post.externfunc(vi,f)
	vi:popScope()
	vi:popName()
end

-----------
function pre.externmethod(vi,f)
	vi:pushName(f.name)
	vi:pushScope()
end

function post.externmethod(vi,f)
	vi:popScope()
	vi:popName()
end
------------
function pre.functype(vi,ft)
	local args=ft.args
	if args then
		spreadVarTypes(args,ft)
		for i,arg in ipairs(args) do
			vi:addDecl(arg)
		end
	end
	
end

function post.functype(vi,ft)

end



------------
function pre.catch(vi,c)
	vi:pushScope()
	for i,v in ipairs(c.vars) do
		vi:addDecl(v)
	end
end

function post.catch(vi,c)
	vi:popScope()
end

------------
function pre.foreachstmt(vi,f)
	vi:pushScope()
	for i,v in ipairs(f.vars) do
		vi:addDecl(v)
	end	
end

function post.foreachstmt(vi,f)
	vi:popScope()
end


function pre.forstmt(vi,f)
	vi:pushScope()
	f.var.type=numberType
	f.var.vtype='local'
	vi:addDecl(f.var)
end

function post.forstmt(vi,f)
	vi:popScope()
end

----------

function pre.classdecl(vi,c)
	c.type=classMetaType
	vi:pushName(c.name)
	vi:pushScope()
end

function post.classdecl(vi,c)
	if c.tvars then
		for i,v in ipairs(c.tvars) do
			vi:addDecl(v)
		end	
	end
	
	if c.decls then
		for i,d in ipairs(c.decls) do
			vi:addDecl(d)
		end
	end
	
	vi:popScope()
	vi:popName()
end

function pre.externclass(vi,c)
	vi:pushName(c.name)
	vi:pushScope()
	if c.decls then
		for i,d in ipairs(c.decls) do
			vi:addDecl(d)
		end
	end
end

function post.externclass(vi,c)
	vi:popScope()
	vi:popName()
end


---
function pre.enumdecl(vi,e)
	vi:pushName(e.name)
	vi:pushScope()
	
	for i,ei in ipairs(e.items) do
		vi:addDecl(ei)
	end
	
end

function post.enumdecl(vi,c)
	vi:popScope()
	vi:popName()

end

