require "lpeg"
require "yu.resolver"
module("yu",package.seeall)

local gen={}

local getOpPrecedence=getOpPrecedence
local getDeclName=getDeclName
local getConst=getConst

local function codegen(m)
	-- print('gen...'..m.tag)
	local t=m.tag
	local g=gen[t]
	if g then
		return g(m) or ''
	else
		error("generator not defined for:"..t)
	end
end

local function codegenList(l)
	local s=''
	if l then
		for i,t in ipairs(l) do
			if i>1 then
				s=s..','
			end
			s=s..codegen(t)
		end
	end
	return s
end

local function codegenEntry(m)
	return codegen(m)
end

local function wrapp(s)
	return '('..s..')'
end

function gen.module(m)
	local b=codegen(m.block)
	return b
end

function gen.block(b)
	local c=''
	for i,s in ipairs(b) do
		local v=codegen(s)
		if v~='' then
			c=c..v..'\n'
		end
	end
	return c
end

-----------------------------CONTROL
function gen.import(im)
	--currentModule.import
end

function gen.private()
end

function gen.public()
end

function gen.rawlua(r)
	return r.src
end


--------------------TYPE
function gen.ttype(t)
	--TODO:!!!!!!!!!!
end

function gen.type(t)
	if isBuiltinType(t.name) then return true end
	local d=findSymbol(t.name,t)
	if not d then return resolveErr("symbol not found:'"..t.id.."'",t ) end
	t.decl=d
	return true
end

function gen.tabletype(t)
	--TODO:!!!!
	
end

function gen.functype(ft)
	-- if ft.args then resolveEach(ft.args) end
	-- if ft.rettype then resolveEach(ft.rettype) end
	-- return true
end


--#STATEMENTS
--#FLOW
function gen.dostmt(d)
	return ' do '..codegen(d.block)..' end '
end

function gen.ifstmt(s)
	local body=' if '..codegen(s.cond)..' then '..codegen(s.body.thenbody)
	if s.body.elsebody then
		body=body..' else '..codegen(s.body.elsebody)
	end
	return body..' end '
end

function gen.switchstmt(s)
	--todo
	local head='do local __SWITCH='..codegen(s.cond)
	local body=''
	for i, c in ipairs(s.cases) do
		body=body..(i>1 and ' elseif ' or ' if ')
		for j, cc in ipairs(c.conds) do
			if j>1 then body=body..' or ' end
			body=body..'__SWITCH=='..codegen(cc)
		end
		body=body..' then '..codegen(c.block)
	end
	if s.default then
		if body~='' then body=body..' else ' end
		body=body..codegen(s.default)
	end
	return head..body..' end end'
end

function gen.case(c)
	--todo
end

function gen.returnstmt(r)
	return 'return '..codegenList(r.values)
end

function gen.yieldstmt(y)
	return '__YUYIELD('..codegenList(r.values)..')'
end

function gen.forstmt(f)
	--todo
	local head='for '..codegen(f.var)..'='..codegenList(f.range)..' do '
	return head..codegen(f.block)..' end'
end

function gen.foreachstmt(f)
	--todo
end

	
function gen.whilestmt(s)
	local out=' while '..codegen(s.cond)..' do '
	
	if s.hasContinue then
		out=out..'local __dobreak=true ' 
		out=out..'repeat '
	end
	
	out=out..codegen(s.block)
	
	if s.hasContinue then
		if s.hasBreak then 
			out=out..'__dobreak=false until false '
			out=out..'if __dobreak then break end'
		else
			out=out..'until true'
		end
	end
	
	out=out..' end '
	return out
end

function gen.continuestmt(c)
	return ' __dobreak=false break '
end

function gen.breakstmt(b)
	return ' break '
end

function gen.trystmt(t)
	--todo:!!!!!!!!!!!!
end

function gen.throwstmt(t)
	--todo:!!!!!!!!!!!!!!
	return ' __YUTHROW('..codegenList(t.values)..') '
end

function gen.catch(c)
	--todo:..............
end

function gen.assignstmt(a)
	return codegenList(a.vars)..' = '..codegenList(a.values)
end

function gen.assopstmt(a)
	--todo:!!!!!!!!
end 

function gen.batchassign(a)
	--todo:!!!!!!!
end


function gen.exprstmt(e) 
	return codegen(e.expr)
end


--#VARIABLES

function gen.vardecl(vd)
	local vtype=vd.vtype
	if vtype=='const' then 
		--no code generation for const
		return ''
	end
	local out=''
	if vtype=='local' then		
		local v=codegenList(vd.vars)
		out=out..'local '..v
		if vd.values then
			out=out..'='..codegenList(vd.values)
		end
	elseif vtype=='global' then
		if vd.values then
			out=codegenList(vd.vars)..'='..codegenList(vd.values)
		end
	elseif vtype=='field' then
		--
	end
	--TODO:global/fields
	return out
end

--#DECLARATION
function gen.tvar(v)
	--todo:!!!!!!
	
end

function gen.var(v)
	local vtype=v.vtype
	if vtype=='local' then
		return v.name
	elseif vtype=='global' then --TODO:get full spaced name
		return v.fullname
	elseif vtype=='field' then
		return v.name
	end
	
end

function gen.arg(a)
	if a.vararg then return '...' end
	return a.name
end

function gen.enumdecl(e)
	-- --todo:assign default const value
	-- resolveEach(e.items)
end

function gen.enumitem(e)
	-- if e.value then resolve(e.value) end
	-- --todo:typecheck
end

function gen.classdecl(c)
	--TODO:!!!!!!!!!!!!!!!!!
	local o=''
	for i,s in ipairs(c.decls) do
		local v=codegen(s)
		if v~='' then
			o=o..v..'\n'
		end
	end
	return o
end


function gen.funcdecl(f)
	--TODO:!!!!!!!!!!!!!!!
	if f.extern then return nil end
	
	local out='function '..getDeclName(f)
	out=out..'('
	if f.type.args then out=out..codegenList(f.type.args) end
	out=out..')\n'
	out=out..codegen(f.block)
	return out..'end'
end

function gen.methoddecl(m)
	-- if m.extern then return nil end
	return gen.funcdecl(m)
	--TODO:!!!!!!!!!!!!!!!1
end

function gen.exprbody(e)
	return ' return '..codegen(e.expr)..' '
end

--#EXPRESSION

-------CONST
 gen["nil"]=function(n)
	return 'nil'
end

function gen.number(n)
	return n.v --raw
end

function gen.string(n)
	--todo: escape?
	return string.format('%q',yu.unescape(n.v))
	-- return '"'..n.v..'"'
end

function gen.boolean(b)
	return b.v
end



function gen.varacc(v)
	local d=v.decl
	return yu.getDeclName(d)
end

function gen.call(c)
	-- local l=codegen(c.l)
	--todo:arguments
	if c.l.tag=='closure' then
		return wrapp(codegen(c.l))..wrapp(codegenList(c.args))
	else
		return codegen(c.l)..wrapp(codegenList(c.args))
	end
end

function gen.spawn(s)
	--TODO:!!!!!!!
end

function gen.new(n)
	--TODO:!!!!!!!!!!
end

function gen.binop(b)
	
	local outl=codegen(b.l)
	local outr=codegen(b.r)
	
	local p=getOpPrecedence(b.op)
	if b.l.tag=='binop' and getOpPrecedence(b.l.op)<p then
		outl=wrapp(outl)
	end
	
	if b.r.tag=='binop' and getOpPrecedence(b.r.op)<p then
		outr=wrapp(outr)
	end
	return outl..' '..b.op..' '..outr
	--TODO:operator override
end

function gen.unop(u)
	return ' '..u.op..' '..wrapp(codegen(u.l))
	--TODO:operator override
end

function gen.ternary(t)
	return wrapp(codegen(t.l)..' and '..codegen(t.vtrue)..' or '..codegen(t.vfalse))
end 

function gen.member(m)
	--TODO:!!!
	local c=getConstNode(m)
	if c then return codegen(c) end
	return codegen(m.l)..'.'..m.id
end

function gen.index(i)
	--TODO:!!!!
	return codegen(i.l)..'['..codegen(i.key)..']'
end

function gen.table(t)
	--TODO:!!!!
end

function gen.item(i)
	--TODO:!!!!
end

function gen.seq(t)
	--TODO:!!!!
end

function gen.cast(a)
	--TODO:!!!! add runtime typecheck
	return codegen(a.l)
end

function gen.is(i)
	--TODO:!!!! 
end

function gen.closure(c)
	--TODO:!!!!
	local out='function '
	out=out..'('
	if c.type.args then out=out..codegenList(c.type.args) end
	out=out..')\n'
	out=out..codegen(c.block)
	return out..'end'
end

function gen.self(s)
	return 'self'
end

function gen.super(s)
	--TODO:!!!!
	return 'self'
end


--------------------EXTERN
function gen.extern(e)
	--TODO:!!!!
	local c=''
	for i,s in ipairs(e.decls) do
		local v=codegen(s)
		if v~=''  then
			c=c..v..'\n'
		end
	end
	return c
end

function gen.externfunc(f)
	--TODO:!!!!
end

function gen.externclass(c)
	--TODO:!!!!
end

function gen.externmethod(m)
	--TODO:!!!!
end
	


_M.codegen=codegenEntry