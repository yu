require "yu.visitor"
require "yu.type"


module("yu",package.seeall)

local is=is
local getType,getTypeDecl,newTypeRef,checkType=getType,getTypeDecl,newTypeRef,checkType
local getExprTypeList=getExprTypeList

local pre={}
local post={}

local __searchSeq=0 

local function _findExternSymbol(entryModule,name,found)
	found=found or {}
	local externModules=entryModule.externModules
	
	if externModules then
		for p,m in pairs(externModules) do
			if m.__seq~=__searchSeq then
				m.__seq=__searchSeq
				local decl=m.scope[name]
				if decl and not decl.private then 
					found[#found+1]={module=m,decl=decl}
				end
				_findExternSymbol(m,name,found)
			end
		end
	end
	
	return found
end

local function _findSymbol(vi,name,token,limit)
	local t=vi.nodeStack.stack
	local outClas,outFunc,outMethod=0,0,0
	
	for i=#t,1,-1 do
		local node=t[i]
		local scope=node.scope
		
		if scope then
			local tag=node.tag
			if tag=='clasdecl' then outClas =outClas +1 end
			if tag=='funcdecl' then outFunc =outFunc +1 end
			if tag=='methoddecl' then outMethod =outMethod +1 end
		
			local decl
			if tag=='classdecl' then
				local clas=node
				while clas do
					decl=clas.scope[name]
					if decl then break end
					clas=clas.superclass
				end
			else
				decl=scope[name]
			end
			
			if decl then 
				-- print('found:',scope,decl.name,node.tag,decl.p0,token.p0)
				if not (
						(limit=='global' and isMemberDecl(decl)) 
				) then
					local dtag=decl.tag
					if dtag=='var' then --variable?
						if decl.vtype=='local' then --local?
							if not ((outFunc>0 and limit~='upvalue') or decl.p0>token.p0) then return decl end
						elseif decl.vtype=='field' then
							if outMethod==1 then return decl end 
						else
							return decl
						end
					elseif dtag=='methoddecl' then
						if outMethod==1 then return decl end 
					else
						return decl
					end
					
					if not (
						decl.tag=='var' and decl.vtype=='local' 
								and (decl.p0>token.p0 or outFunc)
					) then
						
					end
				end
			end
			
		end
		
	end
	
	--search extern modules
	__searchSeq=__searchSeq+1
	
	if not token.module then
		-- print(token.tag)
	end
	
	local found=_findExternSymbol(token.module,name)
	
	local i=0
	local foundCount=#found
	if foundCount>1 then
		msg='multiple declaration found:'..name..'\n'
		for i,f in pairs(found) do
			msg=msg..getTokenPosString(f.decl)..'\n'
		end
		return vi:err(msg,token)
		
	elseif foundCount==1 then
		return found[1].decl
	end
	
	return nil
end

function newResolver()
	local r={	
				pre=pre,
				post=post,
				findSymbol=_findSymbol
			}
	return yu.newVisitor(r)
end

	
----------------TOPLEVEL
	function pre:any(n)
		-- print('resoving..',n.tag)
		local state=n.resolveState
		if state=='done' then 
			return 'skip'
		elseif state=='resolving' then
			self:err('cyclic delcaration dependecy',n)
		end
		
		n.resolveState='resolving'
		
		local b=self.currentBlock
		if b and b.endState then
			self:err('unreachable statement',n)
		end
		
	end
	
	function post:any(n)
		n.resolveState='done'
	end
	
	function pre:module(m)
		m.resolving=true
	end
	
	function post:module(m)
	end
	
	-- function pre:block(b)
		
	-- end
	
	-- function post:block(b)
		
	-- end
	
---------------Directive
	-- function post:import(im)
		-- local m=im.mod
		-- --TODO!
	-- end
	
	-- function post:private()
		-- return true
	-- end
	
	-- function post:public()
		-- return true
	-- end
	
	function post:rawlua(r)
		local f,err=loadstring(r.src)
		if not f then
			self:err('error in Lua code:'..err,r)
		end
	end
	
	------------------------------CONTROL
	function post:exprstmt(e) 
		local expr=e.expr
		if not is(expr.tag,'new','call') then
			self:err("call/new/statement expected, given "..expr.tag,e)
		end
	end

	
	function post:continuestmt(c)
		local l=self:findParentLoop()
		if not l then
			self:err("'continue' must be inside loop block",c)
		end
		l.hasContinue=true
		self.currentBlock.endState=true
		return true
	end

	function post:breakstmt(b)
		local l=self:findParentLoop()
		if not l then
			self:err("'break' must be inside loop block",b)
		end
		l.hasBreak=true
		self.currentBlock.endState=true
		return true
	end
	
	function post:returnstmt(r)
		local f=self:findParentFunc()
		if not f then 
			self:err("'return'/'yield' should be inside function",r)
		end
		self.currentBlock.endState=true
		local rettype=getTypeDecl(f.type.rettype)
		
		if r.values then
			if rettype==voidType then
				if not (#r.values==1 and getType(r.values[1])==voidType) then
					self:err('function should return no value',r)
				end
			elseif rettype.tag=='mulrettype' then
				f.hasReturn=true
				--TODO!!!!!!!!!!!!!!!!!
			else
				f.hasReturn=true
				if #r.values>1 then
					self:err('unexpected return value',r.values[2])
				end
				local rt=getType(r.values[1])
				if not checkType(rettype,'>=',rt) then
					self:err('return type mismatch:'..rettype.name..'<-'..rt.name,r.values[1])
				end
			end
		elseif rettype~=voidType then
			self:err('return value expected',r)
		end
		
		return true
	end
	
	function post:yieldstmt(y,p)
		return post.returnstmt(self,y,p)
	end
	
	-- function post:dostmt(d)
		-- return true
	-- end
	
	function post:ifstmt(s)
		s.cond=convertToBool(s.cond)
		self:visitNode(s.cond)
		return true
	end

	function post:switchstmt(s)
		local condtype=getType(s.cond)
		
		for i,c in ipairs(s.cases) do
			for i,v in ipairs(c.conds) do 
				if not checkType(condtype,'>=',getType(v)) then
					self:err('case expression type mismatch',v)
				end
			end
		end
		
		return true
	end
	
	-- function post:case(c)
		-- return true
	-- end
	
	
	function post:forstmt(f)
		local tt,mulret=getExprTypeList(f.range)
		for i,t in ipairs(tt) do
			if t~=numberType then 
				self:err('for-loop range must be number type, given:'..t.name,f)
			end
		end
	end
	
	function post:foreachstmt(f)
		--todo:!!!!
		--todo:typecheck vars type <=iterator return type
	end
		
	function post:whilestmt(s)
		s.cond=convertToBool(s.cond)
		self:visitNode(s.cond)
		return true
	end
	
	-- function post:trystmt(t)
		-- return true
	-- end
	
	-- function post:throwstmt(t)
		-- return true
	-- end
	
	function post:catch(c)
		--todo:!!!!
		return true
	end
	
-----------------#ASSIGNSTMT
	local function isAssignable(var)
		local tag=var.tag
		
		
		
		if tag=='varacc' or tag=='member' then
			local decl=var.decl
			local dtag=decl.tag
			
			if dtag=='var' then 
				return decl.vtype~='const' 
			end
			
		elseif tag=='index' then
			return true
		end
		return false
	end

	
	function post:assignstmt(a)
		
		local vars,values=a.vars,a.values
		
		for i,var in ipairs(vars) do
			if not isAssignable(var) then
				self:err('non-assignable symbol:'..var.id)
			end
		end
		
		--todo: different variable type(member/index/indexoverride)
		
		--todo typecheck for value/var
		local valtypes,mulretcount=getExprTypeList(values)
		local valcount,varcount=#valtypes,#vars
		
		if valcount<varcount or (valcount-mulretcount+(mulretcount>0 and 1 or 0))>varcount then
			self:err('variable/value count mismatch:'..varcount..'<-'..valcount)
		end
		
		for i,var in ipairs(vars) do
			local vt=valtypes[i]
			local vart=getType(var)
			
			if not checkType(vart,'>=',vt) then
				self:err('variable/value type mismatch:'..vart.name..'<-'..vt.name)
			end
		end
		
		return true
	end
	
	function post:assopstmt(a)
		--todo: check is isAssignable
		--todo: different variable type(member/index/indexoverride)
		-- pendTypeCheck('==',a.var,a.value)
		
		return true
	end 
	
	function post:batchassign(a)
		--todo: replace batch with assigns?
		--todo: check is isAssignable
		--todo: different variable type(member/index/indexoverride)
		--todo typecheck for value/var
		
		-- return 'replace',{
			-- tag='dostmt',
			-- block={tag='block',
				-- {tag='vardecl', vars={
					-- }
			-- }
		-- }
	end

	
-----------------#DECLARATION
	-- function pre:vardecl(vd)
		
	-- end
	
	
	function post:tvar(v)
		--todo:!!!!!!!!!!!!!!!!!!!
		return true
	end
	
	function post:var(v)
		if v.vtype=='const' and not v.value then
			self:err('const variable has no value:'..v.name,v)
		end
		
		local td=getType(v)
		if not td then
			self:err('unresolved type',v)
		else
			v.type=td
		end
		
		if v.value then
			if not checkType(v.type,'>=',getType(v.value)) then 
				self:err('type mismatch',v)
			end
		end
		--TODO:
		return true
	end
	
	function post:arg(a)
	
		return true
	end
	
	function post:enumdecl(e)
		local ii={}
		for i,item in ipairs(e.items) do
			local iv=item.value
			if ii[iv] then self:err('duplicated enum item value',item) end
			ii[iv]=true
		end
		e.type=enumMetaType
	end
	
	function post:enumitem(i,e)
		
		if i.value then
			local v=getConstNode(i.value)
			i.value=v
			if not v or getType(v).tag~='numbertype' then 
				self:err('enum item must be constant number') 
			end
			e.currentValue=v
		else
			local cv=e.currentValue
			if not cv then
				cv=makeNumberConst(1)
			else
				cv=makeNumberConst(tonumber(cv.v)+1)
			end
			i.value=cv
			e.currentValue=cv
		end
		i.type=i.value.type
	end
	
	local function checkConst(n)
		local tag=n.tag
		if is(tag,'number','nil','boolean','string') then return true 
		elseif tag=='var' and n.vtype=='const' then return true 
		elseif tag=='varacc' or tag=='member' then
			if checkConst(n.decl) then return true end
		elseif tag=='binop' then
			if checkConst(n.l) and checkConst(n.r) then return true end
		elseif tag=='unop' then
			return checkConst(n.l)
		end
		return false
	end
	
	function pre:classdecl(c)
		local supername=c.supername
		if supername then
			local s=self:findSymbol(supername.id,c)
			--todo check template
			if not s then
				self:err('symbol not found:'..supername.id,c)
			end
			if s.tag~='classdecl' then self:err(supername.id..'is not a class',c) end
			c.superclass=s
		end
		c.resolveState='done'
	end
	
	function post:classdecl(c)
		------------TODO:!
	end
	
	function pre:methoddecl(m,parent)
		return pre.funcdecl(self,m,parent)
	end
	
	function post:methoddecl(m,parent)
		if m.extern then return end
		local ftype=m.type
		assert(m.block or m.abstract,'abstract?'..m.name)
		if m.inherit then
			local c=parent.superclass
			local superMethod
			while c do
				local m=c.scope[m.name]
				if m then
					if m.tag=='methoddecl' then superMethod=m end
					break
				end
				c=c.superclass
			end
			
			if not superMethod then
				self:err('cannot find method to inherit:'..m.name,m)
			end
		end
		return post.funcdecl(self,m)
	end
	
	function pre:funcdecl(f)
		if f.extern then return end
		if f.block.tag=='exprbody' and f.type.rettype==voidType then
			local expr=f.block.expr
			f.type.rettype=newTypeRef(expr)
		end
		f.resolveState='done'
	end
	
	function post:funcdecl(f)
		if f.extern then return end
		
		local rettype=f.type.rettype
		if not f.hasReturn and rettype.tag~='voidtype' then
			self:err('return statement not found in function block',f)
		end
		return true
	end
	
	function post:exprbody(e)
		self:findParentFunc().hasReturn=true
		--TODO:check type
		return true
	end
----------------------EXPRESSION
	function post:varacc(v)
		local d=self:findSymbol(v.id, v, v.global and 'global' or nil)
		if not d then self:err("symbol not found:'"..v.id.."'",v ) end
		
		if isMemberDecl(d) then 
			return 'replace', {tag='member',l={tag='self'},id=v.id, decl=d, type=getType(d)}
		end
		
		self:visitNode(d)
		
		v.decl=d
		local td=getType(d)
		if td then 
			v.type=td 
		else
			v.type=d.type
			assert(d.type)
		end
		
		return false
	end
	
	local function rateFuncProto(f,c)
		local t=getType(f)
		local cargs=c.args or {}
		local targs=t.args or {}
		
		local ctypes,mulret=getExprTypeList(cargs)
		
		if #ctypes<#targs then
			return 0, 'argument count mismatch', c
		end
		
		local varargType
		local lastArg=targs[#targs] --last func decl arg
		if lastArg and lastArg.vararg then
			varargType=getType(lastArg)
		end
		
		for i,ca in ipairs(cargs) do
			local ta=targs[i]
			local tat
			
			if not ta then
				if not varargType then
					return 0,'argument count mismatch', c
				end
				tat=varargType
			else
				tat=getType(ta)
			end
			
			local cat=getType(ca)
			
			if not checkType(tat,'>=',cat) then 
				return 0, 'argument type mismatch,expecting:'..tat.name..',given:'..cat.name, ca
			end
		end
		--TODO:rate by type matches
		return 1
	end

	local function findCallProto(f,c)
		-- local rates={}
		local topRate=0
		local topProto={}
		
		while f do
			local rate,err,errnode=rateFuncProto(f,c)
			
			if rate==0 then
				
			elseif rate>topRate then
				topRate=rate
				topProto={f}
			elseif rate==topRate then
				topProto[#topProto+1]=f
			end
			
			f=f.nextProto
		end
		
		--TODO:with same rate? order by argument type(the more specific, the better)
		--fixed arg > optional arg > vararg 
		return topProto[1]
	end
	
	
	function post:call(c)
		local f=c.l
		if f.tag=='closure' then
			self:err('cannot call a closure directly',c)
		end
			
		if f.decl and is(f.decl.tag,'funcdecl','methoddecl') then
			
			local proto=findCallProto(f.decl,c)
			if not proto then
				self:err('cannot find match function overload',c)
			end
			f.decl=proto
			f.type=getType(proto)
		end
		
		local lt=getType(c.l)
		yu.resolveCall(lt,c)
		return true
	end
	
	function post:spawn(s)
		local pt=getType(s.proto)
		if pt.tag~='functype' then
			self:err('coroutine proto must be a function,given:'..pt.name,s)
		end
		s.type=pt
		return true
	end
	
	function post:new(n)
		--todo: find constructor proto
		local c=n.class
		local clas=getTypeDecl(c)
		
		if clas.tag~='classdecl' then 
			self:err('instance can be only create from a class',n)
		end
		
		if clas.abstract then
			local msg=''
			for i, m in ipairs(clas.decls) do
				if m.abstract then
					msg=msg..'\n'..m.name
				end
			end
			self:err('class has abstract method(s):'..msg,n)
		end
		
		n.type=n.class
		return true
	end
	
	function post:binop(b)
		local tl=getType(b.l)
		local tr=getType(b.r)
		yu.resolveOP(tl,b)
		return false
	end
	
	function post:unop(u)
		local tl=getType(u.l)
		yu.resolveOP(tl,u)
		return true
	end
	
	function post:ternary(t)
		t.l=convertToBool(t.l)
		
		local tt,tf=getType(t.vtrue),getType(t.vfalse)
		local ts=getSharedSuperType(tt,tf)
		if not ts then
			self:err('ternary result values must be of same type',t)
		end
		t.type=ts
		return true
	end 
	
	function post:member(m)
		local td=getType(m.l)
		
		if not td then
			self:err('unresolved type<member>',m)
		end
		
		resolveMember(td,m)
		
	end
	
	function post:index(i)
		
		local td=getType(i.l)
		if not td then
			self:err('unresolved type<index>',i)
		end
		if not yu.resolveIndex(td,i) then
			self:err('invalid index expression',i)
		end

		return false
	end
	
	function post:table(t)
		local kts={}
		for i,item in ipairs(t.items) do
			kts[#kts+1]=getType(item.key)
		end
		
		local vts={}
		for i,item in ipairs(t.items) do
			vts[#vts+1]=getType(item.value)
		end
		local kt=getSharedSuperType(unpack(kts))
		local vt=getSharedSuperType(unpack(vts))
		
		if not kt then 
			self:err('cannot determine table key type',t)
		end
		if not vt then 
			self:err('cannot determine table value type',t)
		end
		
		t.type={tag='tabletype',etype=vt,ktype=kt,name=vt.name..'['..kt.name..']'}
		return true
	end
	
	function post:item(i)
		return true
	end
	
	-- function pre:seq(t)
		
	-- end
	
	function post:seq(t)
		local vts={}
		if t.items then
			for i,item in ipairs(t.items) do
				vts[#vts+1]=getType(item)
			end
			local vt=getSharedSuperType(unpack(vts))
			
			if not vt then 
				self:err('cannot determine table value type',t)
			end
			
			t.type={tag='tabletype',etype=vt,ktype=numberType,name=vt.name..'[]'}
		else
			--TODO:empty one
			t.type=emptyTableType
		end
		
		return true
	end
	
	function post:cast(a)
		--todo: typecheck l><a.dst
		resolveCast(getType(a.l),a)
	end
	
	function post:is(i)
		--todo: typecheck l>=a.dst
		-- pendTypeCheck('>=',i.l,i.dst)
		
		local lt=getType(i.l)
		if not checkType(lt,'><',i.dst) then
			self:err('unrelated type:'..lt.name..','..i.dst.name)
		end
		
		i.type=booleanType
	end
	
	function pre:closure(c)
		return pre.funcdecl(self,c)
	end
	
	function post:closure(c)
		return post.funcdecl(self,c)
	end
	
	function post:self(s)
		local c=self:findParentClass()
		local f=self:findParentFunc()
		
		if not (c and f and f.tag=='methoddecl') then
			self:err("'self' should be inside method",s)
		end
		s.type=c
		return true
	end
	
	function post:super(s)
		local c=self:findParentClass()
		local f=self:findParentFunc()
		
		if not (c and f and f.tag=='methoddecl') then
			self:err("'super' should be inside method",s)
		end
		local sc=c.superclass
		if not sc then
			self:err("super type not defined",s)
		end
		s.type=sc
		return true
	end
	
---------------------EXTERN
	-- function post:extern(e)
		-- return true
	-- end
	
	-- function post:externfunc(f)
		-- return true
	-- end
	
	-- function post:externclass(c)
		-- return true
	-- end
	
	-- function post:externmethod(m)
		-- return true
	-- end
	
------------------TYPE
	function post:ttype(t) --template type
		local d=self:findSymbol(t.name,t)
		
		if not d or d.tag~='classdecl' then 
			self:err("class not found:'"..t.name.."'",t ) 
		end
		t.class=d
		
		--TODO:check argument
		local args=t.args
		local tvars=d.tvars
		
		local ac=#args
		local tc=#tvars
		
		if ac~=tc then
			self:err("expecting"..tc.."template variable count,given"..ac,t ) 
		end
		for i,a in ipairs(args) do
			if getType(a).tag=='nil' then
				self:err("invalid type for template variable:".."nil",t)
			end
		end
	end
	
	function post:type(t,parent)  --symbol type
		if isBuiltinType(t.name) then 
			t.decl=getBuiltinType(t.name)
			
			if t.decl==anyType then
				local isExternArg=false
				repeat
					if parent.tag~='arg' then break end
					local f=self:findParentFunc()
					if not (f and f.extern) then break end
					isExternArg=true
				until true
				if not isExternArg then 
					self:err("'any' type can only used for extern function argument",t) 
				end
			end
			
			return
		end
		
		local d=self:findSymbol(t.name,t)
		if not (d and isTypeDecl(d)) then self:err("type symbol not found:'"..t.name.."'",t ) end
		t.decl=d
		
	end
	
	function post:tabletype(t)
		--TODO:make proper tabletype name
		t.name='TABLE'
	end
	
	function post.mulrettype(vi,t)
		--TODO:make proper tabletype name
		t.name='(...'..')'
	end
	
	function pre:functype(ft)
		local rt=ft.rettype
		--TODO:make proper functype name
		ft.name='FUNCTION' 
		local ac=#ft.args
		for i=ac,1,-1 do
			local arg=ft.args[i]
			if arg.vararg then
				if i==ac then 
					--ok, vararg
				else
					self:err('only the last argument can be defined as vararg',arg)
				end
			end
		end
	end
	
