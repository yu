local ipairs,pairs=ipairs,pairs
module("yu",package.seeall)

function findModule(m)
	if type(m)=='string' then
		return m
	else
		local src=''
		for i,s in ipairs(m) do
			if i>1 then src=src..'/' end
			src=src..'s'
		end
		return src
	end
end

function unescape(s)
	s=string.gsub(s,'\\(%d%d?%d?)',string.char)
	s=string.gsub(s,'\\.',{
		['\\n']='\n',
		['\\t']='\t',
		['\\r']='\r',
		['\\b']='\b',
		['\\a']='\a',
		['\\v']='\v',
		['\\\\']='\\',
		['\\\'']='\'',
		['\\\"']='\"',
		['\\0']='\0'
	})
	return s
 end
 


	
----TOOL FUNCTIONS
	local stackmt={}
	function stackmt:push(d)
		local s=self.stack
		s[#s+1]=d
		return d
	end
	
	function stackmt:pop()
		local s=self.stack
		local l=#s
		local v=s[l]
		s[l]=nil
		return s[l-1]
	end
	
	function stackmt:peek(k)
		local s=self.stack
		return s[#s-(k or 0)]
	end
	
	function newStack()
		return setmetatable({
			stack={}
		},{__index=stackmt})
	end
	
	
	function findLine(lineInfo,pos)
		local off0=0
		local l0=0
		
		for l,off in ipairs(lineInfo) do
			if pos>=off0 and pos<off then 
				return l0,pos-off0
			end
			off0=off
			l0=l
		end
		return l0,pos-off0
	end
	
	function getTokenPosString(token,currentModule)
		local pos=token.p0 or 0
		local m=token.module or currentModule
		local line,lpos=findLine(m.lineInfo,pos)
		return "@"..m.file.."<"..line..":"..lpos..">"
	end
	
	function compileErr(msg,token,currentModule)
		local errmsg="error"..(token and getTokenPosString(token,currentModule) or "")..":"..msg
		print('----------COMPILE ERROR:')
		print(errmsg)
		return error('compile error')
	end

	
	
	function is(i,a,b,...)
		if i==a then return true end
		if b then return is(i,b,...) end
		return false
	end

---------------------Token types		
	function isConst(c)
		return is(c.tag,'number','boolean','string','nil')
	end
	
	function isDecl(b)
		local tag=b.tag
		return is(tag,"vardecl" ,"classdecl" ,"funcdecl" ,"enumdecl" ,"methoddecl" 
		,"var","externfunc","externclass")
	end
	
	function isGlobalDecl(b)
		local tag=b.tag
		if tag=="vardecl" then
			local vtype=b.vtype
			return vtype=="global" or vtype=="const"
		else 
			return is(tag, "classdecl" ,"funcdecl" ,"enumdecl" ,"methoddecl")
		end
	end
	
	function isMemberDecl(d)
		local tag=d.tag
		return ((tag=='vardecl' or tag=='var') and d.vtype=='field') or tag=='methoddecl'
	end
	
	function isTypeDecl(t)
		return is(t.tag,'classdecl','enumdecl','functype')
	end
	
	local function findExternModule(src,dst,checked)
		if src==dst then return true end
		checked[src]=true
		for path,m in pairs(src.externModules) do
			if not checked[m] and findExternModule(m,dst,checked) then 
				return true
			end
		end
		return false
	end
	
	local _visibleCache={}
	function isModuleVisible(src,dst)
		local vis=src.visibleMods
		if not vis then vis={} src.visibleMods=vis end
		local v=vis[dst]
		if v~=nil then return v end
		v=findExternModule(src,dst,{})
		vis[dst]=v
		return v
	end
	
	local function getConstNode(n)
		local tag=n.tag
		if is(tag,'string','nil','number') then return n end
		if tag=='var' and n.vtype=='const' then return getConstNode(n.value) end
		if is(tag,'varacc','member') then
			local decl=n.decl
			if decl then return getConstNode(decl) end
		end
		if tag=='enumitem' then
			return getConstNode(n.value)
		end
		
		return nil
	end
	
	local function getConst(n)
		local tag=n.tag
		if tag=='string' then return string.format('%q',yu.unescape(n.v)) end
		if tag=='nil' then return 'nil' end
		if tag=='number' or tag=='boolean' then return n.v end
		error('non const:'..tag)
	end

	local function getDeclName(d)
		local tag=d.tag
		if tag=='var' then
			local vtype=d.vtype
			if vtype=='local' then return d.name end
			if vtype=='global' then return d.fullname end
			if vtype=='const' then return getConst(d.value) end
			if vtype=='field' then 
				--TODO: self?
				return d.fullname 
			end
		elseif tag=='funcdecl' then
			--TODO:....
		elseif tag=='arg' then
			return d.name
		end
		return d.fullname
	end
	
	
local opPrecedence={
	{'and','or'},
	{'>','<','>=','<=','==','~='},
	{'..'},
	{'+','-'},
	{'*','/','^','%'}
}

local opClass={
	['+']='arith',
	['-']='arith',
	['*']='arith',
	['/']='arith',
	['%']='arith',
	['^']='arith',
	
	['..']='concat',
	
	['>']='order',
	['<']='order',
	['>=']='order',
	['<=']='order',
	
	['==']='equal',
	['~=']='equal',
	
	['and']='logic',
	['or']='logic',
	['not']='logic',
}

function getOpPrecedence(op)
	for i,s in ipairs(opPrecedence) do
		for j,o in ipairs(s) do
			if op==o then return i end
		end
	end
	return error("unknown op:"..op)
end

function getOpClass(op)	
	return opClass[op]
end

_M.getConstNode=getConstNode
_M.getConst=getConst
_M.getDeclName=getDeclName


	