require 'yu.utils'

local is,type=yu.is,type

module ("yu",package.seeall)


nilType={tag='niltype',name='nil'}
booleanType={tag='booleantype',name='boolean'}
numberType={tag='numbertype',name='number'}
stringType={tag='stringtype',name='string'}
objectType={tag='objecttype',name='object'}
voidType={tag='voidtype',name='void'}
anyType={tag='anytype',name='any'}

classMetaType={tag='classmeta',name='class'}
moduleMetaType={tag='modulemeta',name='module'}
enumMetaType={tag='enummeta',name='enum'}
funcMetaType={tag='funcmeta',name='func'}
tableMetaType={tag='tablemeta',name='table'}

emptyTableType={tag='tabletype',name='[]'}

stringTable={}
numberTable={}

trueConst={tag='boolean', v='true', type=booleanType}
falseConst={tag='boolean', v='false',type=booleanType}
nilConst={tag='nil',type=nilType}


function makeNumberConst(v)
	n=tostring(v)
	local c=numberTable[n]
	if not c then
		c={tag='number', v=tostring(n), type=numberType ,resolveState='done'}
		numberTable[n]=c
	end
	return c
end

function makeStringConst(s)
	local c=stringTable[s]
	if not c then
		c={tag='string', v=s, type=stringType, resolveState='done'}
		stringTable[s]=c
	end
	return c
end


local function getSuperType(t)
	--TODO:
	local tag=t.tag
	if tag=='classdecl' then
		local super=t.superclass
		if super then return super end
		return objectType
	end
	return nil
end

local function getFirstSuperType(t)
	if t.tag=='classdecl' then return objectType end
	
	local s=t
	while true do
		local s1=getSuperType(s)
		if not s1 then break end
		s=s1
	end
	return s
end

local function getSuperTypeList(t)
	local list={t}
	local s=t
	while true do
		local s1=getSuperType(s)
		if not s1 then break end
		list[#list+1]=s1
		s=s1
	end
	return list
end

local function getSharedSuperType(a,b,c,...)
	if not b then return a end
	local asl,bsl=getSuperTypeList(a),getSuperTypeList(b)
	for i,as in ipairs(asl) do
		for j,bs in ipairs(bsl) do
			if as==bs then
				if c then
					return getSharedSuperType(as,c,...)
				else
					return as
				end
			end
		end
	end
	return nil
end

local function isSuperType(t1,t2)
	if t1==anyType then return true end
	
	if t1.tag=='tabletype' and t2==emptyTableType then
		
		return true
	end
	
	local s=t2
	while true do
		local s1=getSuperType(s)
		if not s1 then break end
		if s1==t1 then return true end
		s=s1
	end
	return false
end

local function isSameType(t1,t2)
	-- assert(t1 and t2)
	local d1,d2=getTypeDecl(t1),getTypeDecl(t2)
	if d1==d2 then return true end
	local tag1,tag2=d1.tag,d2.tag
	if tag1~=tag2 then return false end
	if tag1=='tabletype' then
		-- table.foreach(d1,print)
		return isSameType(d1.ktype,d2.ktype) and 
				isSameType(d1.etype,d2.etype)
	end
	return true
end


-- '>' t1 is super of t2, vice vesa
-- '==' t1 is same as t2
-- '>=' t1 is super of t2 or t1 is same as t2, vicevesa
-- '~=' t1 is not the same as t2
-- '><' t1 and t2 has same origin(super)
-- 'ret' t1 is return type of t2(func)
-- '->' t1 can be converted into t2
function checkType(t1,m,t2)
	t1,t2=getTypeDecl(t1),getTypeDecl(t2)
	
	if m=='>' then
		result=isSuperType(t1,t2)
	elseif m=='>=' then
		result=isSuperType(t1,t2) or isSameType(t1,t2)
	elseif m=='==' then
		result=isSameType(t1,t2)
	elseif m=='~=' then
		result= not isSameType(t1,t2)
	elseif m=='<' then
		result=isSuperType(t2,t1)
	elseif m=='<=' then
		result=isSuperType(t1,t2) or isSameType(t1,t2)
	elseif m=='><' then
		local s1=getFirstSuperType(t1)
		local s2=getFirstSuperType(t2)
		
		result= isSameType(s1,s2)
	end
	return result
end

function castType(node,targetType)
	local td=getTypeDecl(node)
	if td==targetType then return node end
	--TODO:
	
end



local builtinTypeDecls={
	
	['nil']=nilType,
	['boolean']=booleanType,
	['number']=numberType,
	['string']=stringType,
	['object']=objectType,
	['@void']=voidType,
	['any']=anyType,
	
	['classdecl']=classMetaType,
	['import']=moduleMetaType,
	['enumdecl']=enumMetaType,
	['functype']=funcMetaType,
	['tabletype']=tableMetaType
	
}

function isBuiltinType(n)
	return is(n,'boolean','number','nil','string','object','any')
end

function getBuiltinType(name)
	return builtinTypeDecls[name]
end


local function getTypeDecl(d)
	-- print('getting type',d.tag)
	if isTypeDecl(d) then return d end
	
	local tag=d.tag
	if tag=='type' then
		
		if not d.decl then
			table.foreach(d,print)
			error("unresolved typenode")
		end
		
		return d.decl
	elseif tag=='ttype' then
		--TODO:
		error('todo')
	else
		table.foreach(d,print)
		print(getTokenPosString(d))
		error('invalid type')
	end
		
end

local function getType(node)
	local t=node.type
	if not t then
		print(node)
		table.foreach(node,print)
		error('unresolved node')
	end
	
	if t.tag=='typeref' then
		return getType(t.ref)
	end
	
	return getTypeDecl(t)
end


 function getExprTypeList(values) --for expanding multiple return
	local types={}
	local c=#values
	local mulret=0
	
	for i=1,c do
		local v=values[i]
		local t=getType(v)
		if t.tag=='mulrettype' then
			if i==c then --lastone
				for j,rt in ipairs(t.types) do
					types[#types+1]=getTypeDecl(rt)
				end
				mulret=#t.types
			else
				types[#types+1]=t.types[1]
			end
		else
			types[#types+1]=t
		end
	end
	return types,mulret
end

function isTypeDecl(n)
	local tag=n.tag
	return is(n.tag,
		'classdecl','enumdecl','functype','tabletype','voidtype','mulrettype',
		'niltype','booleantype','numbertype','stringtype','externclass','anytype',
		'objecttype','classmeta','enummeta','funcmeta','tablemeta','modulemeta'
	)
end

function newTypeRef(n)
	return {tag='typeref',ref=n}
end



------CLASS TEMPLATE
local tvarKeys={}
function getTVarKey(args)
	local t=tvarKeys
	local tt=nil
	for i,a in ipairs(args) do
		tt=t[a]
		if not tt then
			tt={}
			t[a]=tt
		end
	end
	return tt
end

local function getClassInstance(c,args)
	assert(c.tvars)
	local instances=c.instances
	if not instances then instances={} c.instances=instances end
	local key=getTVarKey(args)
	
	local i=instances[key]
	if not i then 
		--replace tvar with real type
		
	end
	
	return i
end

local typeres={
	op={},
	index={},
	member={},
	call={},
	cast={}
	
}


----------OP
function typeres.op.niltype(t,n)
	local op=n.op
	local cls=yu.getOpClass(op)
	
	if cls~='equal' and cls~='logic' then
		compileErr('cannot perform op '..op..' on type: nil',n)
	end
	
	n.type=booleanType
	return true 
	--TODO:convert to boolean?
end

function typeres.op.booleantype(t,n)
	local op=n.op
	local cls=yu.getOpClass(op)
	
	if cls~='equal' and cls~='logic' then
		compileErr('cannot perform op '..op..' on type: nil',n)
	end
	
	n.type=booleanType
	return true 
end


function typeres.op.numbertype(t,n)
	local op=n.op
	
	if n.tag=='binop' then
		local cls=yu.getOpClass(op)
		local tr=getType(n.r)
		if cls=='arith' then
			if tr.tag~='numbertype' then
				compileErr('cannot perform arith op on type:'..tr.name,n.r)
			end
			n.type=numberType
		elseif cls=='order' then
			if tr.tag~='numbertype' then
				compileErr('cannot perform order op on type:'..tr.name,n.r)
			end
			n.type=booleanType
		elseif cls=='concat' then
			n.type=stringType
		elseif cls=='equal' then
			n.type=booleanType
		end
		return true 
	else
		if op=='-' then
			n.type=numberType
		elseif op=='not' then
			n.type=booleanType
		end
		return true
	end
end

function typeres.op.stringtype(t,n)
	local op=n.op
	if n.tag=='binop' then
		local cls=yu.getOpClass(op)
		local tr=getType(n.r)
		if cls=='concat' then
			if not is(tr.tag,'numbertype','stringtype') then
				compileErr('cannot perform concat op on type:'..tr.name,n.r)
			end
			n.type=stringType
		elseif cls=='order' then
			if tr.tag~='stringtype' then
				compileErr('cannot compare types:'..t.name..','..tr.name,n.r)
			end
			n.type=booleanType
		elseif cls=='equal' then
			n.type=booleanType
		elseif cls=='arith' then
			compileErr('cannot perform arith op on string type',n)
		end
	else
		if op=='not' then
			n.type=booleanType
		elseif op=='-' then
			compileErr('cannot perform arith op on string type',n)
		end
	end
end

function typeres.op.classdecl()--object
	--TODO:operator override
end



----------MEMBER
local function getClassMemberDecl(c,id, fromModule)
	local scope=c.scope
	local d=scope[id]
	
	if d and not (fromModule~=c.module and d.private) then
		local dtag=d.tag
		local ismember=false
		
		if dtag=='methoddecl' then 
			ismember=true
		elseif dtag=='var' then
			ismember=d.vtype=='field'
		end
		
		return d,ismember
	end
	
	if c.superclass then return getClassMemberDecl(c.superclass,id,fromModule) end
	
	return nil,false
end

function typeres.member.classmeta(t,m)
	local c=m.l.decl
	local d,ismember=getClassMemberDecl(c,m.id,m.module)
	
	if d and not ismember then
		m.decl=d
		m.type=getType(d)
		return true
	end
	
	compileErr('static member not found:'..m.id,m)
end

function typeres.member.enummeta(t,m)
	local e=m.l.decl
	local item=e.scope[m.id]
	
	if item then
		m.decl=item
		m.type=getType(item)
		return true
	end
	
	compileErr('enum item not found:'..m.id,m)
end

function typeres.member.modulemeta(t,m)
	local im=m.l.decl
	local mod=im.mod
	local item=mod.scope[m.id]
	
	if item and not item.private then
		m.decl=item
		m.type=getType(item)
		return true
	end
	
	compileErr('symbol not found:'..m.id,m)
end

function typeres.member.classdecl(t,m)
	local d,ismember=getClassMemberDecl(t,m.id,m.module)
	
	if ismember then
		m.decl=d
		m.type=getType(d)
		return true
	end
	
	compileErr('member not found:'..m.id,m)
end

typeres.member.externclass=typeres.member.classdecl


function typeres.member.ttype(t,m)
	local c=t.class
	local d,ismember=getClassMemberDecl(c,m.id)
	
	if not ismember then return false end
end

-----------INDEX
function typeres.index.tabletype(t,idx)
	local kt=getType(idx.key)
	local akt=t.ktype
	
	if not checkType(akt,'>=',kt) then
		compileErr('index key type mismatch,expecting:'..akt.name..', given:'..kt.name,idx)
	end
	
	idx.type=t.etype
	return true
end

-------CALL

function typeres.call.functype(t,c)
	local rettype=t.rettype
	if not rettype then
		c.type=voidType
	elseif rettype.tag=='mulrettype' then
		c.type=rettype --TODO:?? multiple return
	else
		c.type=rettype
	end
end


-------CAST
function typeres.cast.numbertype(t,n,target)
	--TODO:...
	if target==booleanType then
		return {
			tag='binop',
			op='~=',
			l=n,
			r=makeNumberConst(0),
			resolveState='done'
		}
	elseif target==stringType then
		return n
	end
end

function typeres.cast.stringtype(t,n,target)
	--TODO:...
	if target==numberType then
	elseif target==booleanType then
		
	end
end

function typeres.cast.niltype(t,n,target)
	if target==booleanType then
		return falseConst
	end
	return false
end
-------------

-------ENTRY
function resolveMember(t,node)
	local tag=t.tag
	local r=typeres.member[tag]
	
	if not r then
		-- table.foreach(node.l.type,print)
		compileErr('no member for type:'..t.name,node)
	end
	
	return r(t,node)
end

function resolveOP(t,node)
	local tag=t.tag
	local r=typeres.op[tag]
	
	if not r then
		compileErr('cannot perform op \''..node.op..'\' on type:'..t.name,node)
	end
	
	return r(t,node)
end

function resolveIndex(t,node)
	local tag=t.tag
	local r=typeres.index[tag]
	if not r then
		compileErr('cannot perform index on type:'..t.name,node)
	end
	return r(t,node)
end

function resolveCall(t,node)
	local tag=t.tag
	local r=typeres.call[tag]
	if not r then
		compileErr('cannot call type:'..t.name,node)
	end
	return r(t,node)
end

function resolveCast(t,node)
	local tag=t.tag
	local dst=node.dst
	if checkType(t,'<=',dst) then
		node.type=dst
		return true
	end
	
	local r=typeres.cast[tag]
	if not r then
		compileErr('cannot cast type:'..t.name,node)
	end
	
	return r(t,node,dst)
end

function convertToBool(node)
	local t=getType(node)
	local tag=t.tag
	
	if t==nilType then
		return falseConst
	elseif t==booleanType then
		return node
	elseif t==numberType then
		return {tag='binop',op='~=',l=node,r=makeNumberConst(0), type=booleanType}
	elseif t==stringType then
		---TODO:
	elseif tag=='classdecl' then
		return {tag='binop',op='~=',l=node,r=nilConst,type=booleanType}
	else
		compileErr('cannot convert into boolean type:'..t.name,node)
	end
	
end

---------------------------

-- _M.isSameType=isSameType
-- _M.isSuperType=isSuperType
_M.getTypeDecl=getTypeDecl
_M.getType=getType
_M.builtinTypeDecls=builtinTypeDecls
_M.getSharedSuperType=getSharedSuperType
